// import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
// import { AuthGuard } from 'src/guards/auth.guard';
// import { RolesGuard } from 'src/guards/roles.guard';

// export function Auth(...roles: Role[]) {
//   return applyDecorators(
//     SetMetadata('roles', roles),
//     UseGuards(AuthGuard, RolesGuard),
//     ApiBearerAuth(),
//     ApiUnauthorizedResponse({ description: 'Unauthorized"' }),
//   );
// }
//use:
// @Get('users')
// @Auth('admin')
// findAllUsers() {}
