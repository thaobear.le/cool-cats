import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { LoggerMiddleware } from './middlewares/logger.middleware';
import { ValidationPipe } from './pipes/validation.pipe';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // app.use(LoggerMiddleware);
  // app.useGlobalPipes(new ValidationPipe());
  //  bind middleware to every registered route

  app.enableCors();
  await app.listen(8888);
}
bootstrap();
