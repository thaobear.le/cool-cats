import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';
import { Cat } from './entity/cat.entity';
import { CreateCatDto } from './dto/create-cat.dto';
import { isUndefined } from 'util';

@Injectable()
export class CatsService {
  constructor(
    @InjectRepository(Cat)
    private catsRepository: Repository<Cat>,
    private connection: Connection,
  ) {}

  findAll(): Promise<Cat[]> {
    return this.catsRepository.find();
  }

  async findOne(id: string): Promise<Cat | null> {
    // try (return this.catsRepository.findOneOrFail(id);)
    const res = await this.catsRepository.findOne(id);
    return res;
  }

  async create(cat: CreateCatDto): Promise<Cat> {
    return await this.catsRepository.save(cat);
  }
  async update(id: string, newValue: CreateCatDto): Promise<Cat | null> {
    const cat = await this.catsRepository.findOne(id);
    if (isUndefined(cat)) {
      console.error("Cat doesn't exist");
    }
    this.catsRepository.update(id, newValue);
    return this.catsRepository.findOne(id);
  }
  async remove(id: string): Promise<void> {
    await this.catsRepository.delete(id);
  }
}
