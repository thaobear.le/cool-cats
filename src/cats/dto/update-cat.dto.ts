import { IsString, IsInt, IsUUID, IsEmail } from 'class-validator';

export class UpdateCatDto {
  @IsEmail()
  email: string;

  @IsString()
  lastName: string;

  @IsString()
  firstName: string;
  @IsString()
  avatar: string;
}
