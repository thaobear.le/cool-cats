import {
  IsString,
  IsInt,
  IsUUID,
  IsEmail,
  IsAlpha,
  isURL,
  IsUrl,
} from 'class-validator';

export class CreateCatDto {
  @IsEmail()
  email: string;

  @IsString()
  lastName: string;

  @IsString()
  firstName: string;
  @IsString()
  avatar: string;
}
