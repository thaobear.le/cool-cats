export interface Cat {
  id: string;
  email: string;
  lastName: string;
  firstName: string;
  avatar: string;
}
