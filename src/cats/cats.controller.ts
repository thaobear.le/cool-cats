import {
  Controller,
  UploadedFile,
  Get,
  Put,
  Delete,
  Req,
  Post,
  HttpCode,
  Redirect,
  Param,
  HostParam,
  Body,
  UseFilters,
  ForbiddenException,
  UsePipes,
  UseGuards,
  SetMetadata,
  UseInterceptors,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { UpdateCatDto } from './dto/update-cat.dto';
import { Cat } from './interfaces/cat.interface';
import { CatsService } from './cats.service';
import { ValidationPipe } from 'src/pipes/validation.pipe';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from '../utils/file-uploading.utils';
import { CreateCatDto } from './dto/create-cat.dto';
import { isUndefined, isBoolean } from 'util';
import { DeleteResult } from 'typeorm';
@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Get()
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @Get('image/:imgpath')
  seeUpLoadedFile(@Param('imgpath') image, ctx: ExecutionContext) {
    const response = ctx.switchToHttp().getResponse();
    return response.sendFile(image, { root: './files' });
  }
  @Post()
  async create(@Body(ValidationPipe) createCatDto: CreateCatDto) {
    return this.catsService.create(createCatDto);
  }

  @Post('image')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './files',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async uploadedFile(@UploadedFile() image) {
    const response = {
      originalname: image.originalname,
      filename: image.filename,
    };
    return response;
  }

  @Get(':id')
  async findOne(@Param('id') id): Promise<Cat> {
    const res = await this.catsService.findOne(id);
    if (!isUndefined(res)) {
      return res;
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: "Cat doesn't exist",
        },
        404,
      );
    }
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body(ValidationPipe) updateCatDto: UpdateCatDto,
  ) {
    const res = await this.catsService.update(id, updateCatDto);

    if (!isUndefined(res)) {
      return res;
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: "Cat doesn't exist",
        },
        404,
      );
    }
  }

  @Delete(':id')
  remove(@Param('id') id): Promise<void> {
    return this.catsService.remove(id);
  }
}
