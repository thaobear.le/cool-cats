import { Module, Global } from '@nestjs/common';
import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';
import { Cat } from './entity/cat.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
// @Global() CatsService provider will be ubiquitous, and modules that wish to inject the service will not need to import the CatsModule in their imports array.
@Module({
  imports: [TypeOrmModule.forFeature([Cat])],
  controllers: [CatsController],
  providers: [CatsService],
  exports: [CatsService, TypeOrmModule],
})
export class CatsModule {}
